#ifndef JSONUTILS_H
#define JSONUTILS_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

QString jsonObjectToString(const QJsonObject & jobj);
QString jsonArrayToString(const QJsonArray & jarr);
QJsonDocument stringToJsonDocument(const QString & str);


#endif // JSONUTILS_H
