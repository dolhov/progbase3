#pragma once

#include <string>
#include <vector>

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QFileInfo>
#include <QVariant>
#include <QDebug>

#include <optional>
#include "tour_operators.h"
#include "vacation.h"
#include "storage.h"
#include <QSqlError>

using namespace std;



class SqliteStorage : public Storage
{
    int user_loged_id;
 protected:
   QSqlDatabase database_;
   QString hashPassword(QString const & pass);
 public:
   SqliteStorage(const string & dir_name);

   bool isOpen() const;
   bool open();
   void close();

   // students
   vector<tour_operator> getAllTourOperators(void);
   vector<tour_operator> getSomeTourOperators(unsigned int page_size,unsigned int page_number);
   optional<tour_operator> getTourOperatorById(int operator_id);
   bool updateTourOperator(const tour_operator &tour_op);
   bool removeTourOperator(int operator_id);
   int insertTourOperator(const tour_operator &tour_op);

   // vacatons
   vector<vacation> getAllVacations(void);
   optional<vacation> getVacationById(int vacation_id);
   bool updateVacation(const vacation & vacation_upd);
   bool removeVacation(int vacation_id);
   int insertVacation(const vacation &vac);


   //users
   optional<User> getUserAuth(
       const string & username,
       const string & password);
   vector<tour_operator> getAllUserTourOperators(int user_id);
   vector<tour_operator> getSomeUserTourOperators(int user_id, unsigned int limit,unsigned int skipped_items);
   vector<tour_operator> searchSomeUserTourOperators(int user_id,unsigned int limit, unsigned int skipped_items, enum Attributes atr, std::string parameter);
   vector<vacation> searchSomeTourOperatorVacations(int tourop_id, unsigned int limit,unsigned int skipped_items, enum VacationAttributes atr, std::string parameter);
   int numberOfUserTourOperators(int user_id, enum Attributes atr = Attributes::EMPTY, std::string parameter = "");
   int insertUser(std::string username, std::string password);
   bool isUniqueUser(string username);

   // links
   vector<vacation> getAllTourOperatorsVacations(int tourop_id);
   bool insertTourOperatorVacation(int tourop_id, int vacation_id);
   bool removeTourOperatorVacation(int tourop_id, int vacation_id);
   bool removeAllTourOperatorVacation(int tourop_id);

};
