#include "jsonutils.h"

QString jsonObjectToString(const QJsonObject & jobj)
{
   QJsonDocument doc;
   doc.setObject(jobj);
   return doc.toJson();
}
QString jsonArrayToString(const QJsonArray & jarr)
{
   QJsonDocument doc;
   doc.setArray(jarr);
   return doc.toJson();
}
QJsonDocument stringToJsonDocument(const QString & str)
{
   QJsonParseError err;
   QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8(), &err);
   if (err.error != QJsonParseError::NoError) { throw err; }
   return doc;
}
