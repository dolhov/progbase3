#pragma once
#include <string>

struct tour_operator
{
    int id;
    std::string name;
    std::string city;
    int cost;
    std::string vacation;
    std::string currency;
    int user_id;
};
