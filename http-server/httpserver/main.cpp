#include <QCoreApplication>
#include "httpserver.h"
#include "jsonutils.h"
#include "models.h"
#include "sqlitestorage.h"

optional<User> userAuth(HttpRequest & req, SqliteStorage* storage);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
     SqliteStorage storage("../../data/sql/data_sqlite");
     storage.open();
     HttpServer http_server;
     http_server.get("/", [&](HttpRequest & req, HttpResponse & res){
        res.body = "Root Endpoint!";
     });

     http_server.get("/tour_operators", [&](HttpRequest & req, HttpResponse & res){
        optional<User> opt = userAuth(req, &storage);
        if (opt)
        {
            QVector<tour_operator> tour_ops;
            tour_ops = tour_ops.fromStdVector(storage.getAllUserTourOperators(opt.value().id));
            res.setContentType("text/json");
            res.body += jsonArrayToString(TourOperatorsToJson(tour_ops));
        }
        else
        {
            res.status_code = 401;
            res.status_description = "Unauthorized";
        }
     });

     http_server.get("/tour_operators", [&](HttpRequest & req, HttpResponse & res){
        int attribute = req.query["attribute"].toInt();
        QString parameter = req.query["parameter"];
        int limit = req.query["limit"].toInt();
        int offset = req.query["offset"].toInt();
        QVector<tour_operator> tour_ops;
        QString username;
        QString password;
        QString st = QString::fromStdString(req.headers["Authorization"].toStdString());
        if (st.startsWith("Basic "))
        {
            QStringList list;
            list = st.split(" ");
            st = list[1];
            QByteArray arr;
            arr = arr.fromBase64(QByteArray::fromStdString(st.toStdString()));
            st = st.fromStdString(arr.toStdString());
            list = st.split(":");
            username = list[0];
            password = list[1];
        }

        //storage.getUserAuth();
        optional<User> opt = storage.getUserAuth(username.toStdString(), password.toStdString());
        if (opt)
        {
            tour_ops = tour_ops.fromStdVector(storage.searchSomeUserTourOperators(opt.value().id, limit, offset, static_cast<Attributes>(attribute), parameter.toStdString()));
            res.setContentType("text/json");
            res.body = jsonArrayToString(TourOperatorsToJson(tour_ops));
        }
        else
        {
            res.status_code = 401;
            res.status_description = "Unauthorized";
        }
     });

     http_server.get("/users", [&](HttpRequest & req, HttpResponse & res)
     {
         QString username;
         QString password;
         QString st = QString::fromStdString(req.headers["Authorization"].toStdString());
         if (st.startsWith("Basic "))
         {
             QStringList list;
             list = st.split(" ");
             st = list[1];
             QByteArray arr;
             arr = arr.fromBase64(QByteArray::fromStdString(st.toStdString()));
             st = st.fromStdString(arr.toStdString());
             list = st.split(":");
             username = list[0];
             password = list[1];
         }
         if (storage.getUserAuth(username.toStdString(), password.toStdString()))
         {
            res.setContentType("text/json");
         }
         else
         {
             res.status_code = 401;
             res.status_description = "Unauthorized";
         }
     });

     http_server.post("/tour_operators/:id", [&](HttpRequest & req, HttpResponse & res){  // POST
         int id = req.params["id"].toInt();
         int user_id = userAuth(req, &storage).value().id;
         if (user_id != -1)
         {
             if (storage.getTourOperatorById(id).value().user_id == user_id)
             {
                 tour_operator tour_op;
                 tour_op.name = req.query["name"].toStdString();
                 tour_op.city = req.query["city"].toStdString();
                 tour_op.cost = req.query["cost"].toInt();
                 tour_op.vacation = req.query["vacation"].toStdString();
                 tour_op.currency = req.query["currency"].toStdString();
                 tour_op.id = id;
                 storage.updateTourOperator(tour_op);
             }
             else
             {
                 res.status_code = 401;
                 res.status_description = "Unauthorized";
             }
         }
         else
         {
             res.status_code = 401;
             res.status_description = "Unauthorized";
         }
     });
     http_server.delete_("/tour_operators/:id", [&](HttpRequest & req, HttpResponse & res)
     {
         int id = req.params["id"].toInt();
         int user_id = userAuth(req, &storage).value().id;
         if (user_id != -1)
         {
             if (storage.getTourOperatorById(id).value().user_id == user_id)
             {
                 storage.removeAllTourOperatorVacation(id);
                 if(!storage.removeTourOperator(id))
                 {
                     res.status_code = 500;
                     res.status_description = "Internal server error";
                 }
             }
             else
             {
                 res.status_code = 401;
                 res.status_description = "Unauthorized";
             }
         }
         else
         {
             res.status_code = 401;
             res.status_description = "Unauthorized";
         }
     });
     http_server.post("/tour_operators", [&](HttpRequest & req, HttpResponse & res)
     {
         int user_id = userAuth(req, &storage).value().id;
         if (user_id != -1)
         {
             tour_operator tour_op;
             tour_op.name = req.query["name"].toStdString();
             tour_op.city = req.query["city"].toStdString();
             tour_op.cost = req.query["cost"].toInt();
             tour_op.vacation = req.query["vacation"].toStdString();
             tour_op.currency = req.query["currency"].toStdString();
             tour_op.user_id = user_id;
             storage.insertTourOperator(tour_op);
         }
         else
         {
             res.status_code = 401;
             res.status_description = "Unauthorized";
         }
     });
     http_server.post("/users", [&](HttpRequest & req, HttpResponse & res)
     {
         QString username;
         QString password;
         QString st = QString::fromStdString(req.headers["Authorization"].toStdString());
         if (st.startsWith("Basic "))
         {
             QStringList list;
             list = st.split(" ");
             st = list[1];
             QByteArray arr;
             arr = arr.fromBase64(QByteArray::fromStdString(st.toStdString()));
             st = st.fromStdString(arr.toStdString());
             list = st.split(":");
             username = list[0];
             password = list[1];
         }
         if(!storage.isUniqueUser(username.toStdString()))
         {
             res.status_code = 403;
             res.status_description = "Forbidden";
         }
         else
         {
             storage.insertUser(username.toStdString(), password.toStdString());
         }
     });
     http_server.get("/tour_operators/:id/vacations", [&](HttpRequest & req, HttpResponse & res)
     {
         int id = req.params["id"].toInt();
         int limit = req.query["limit"].toInt();
         int offset = req.query["offset"].toInt();
         VacationAttributes atr = static_cast<VacationAttributes>(req.query["attribute"].toInt());
         string parameter = req.query["parameter"].toStdString();
         int user_id = userAuth(req, &storage).value().id;
         if (user_id != -1)
         {
             vector<vacation> vacs = storage.searchSomeTourOperatorVacations(id, limit, offset, atr, parameter);
             QVector<vacation> qvacs;
             qvacs = qvacs.fromStdVector(vacs);
             res.setContentType("text/json");
             res.body += jsonArrayToString(VacationsToJson(qvacs));
         }
         else
         {
             res.status_code = 401;
             res.status_description = "Unauthorized";
         }
     });

     http_server.post("/tour_operators/:id/vacations", [&](HttpRequest & req, HttpResponse & res)
     {

         int id = req.params["id"].toInt();
         int vac_id = req.query["id"].toInt();
         optional<User> opt = userAuth(req, &storage);
         optional<tour_operator> tourop = storage.getTourOperatorById(id);
         if (!tourop)
         {
             res.status_code = 404;
             res.status_description = "Not found";
         }
         else
         {
             if(opt)
             {
                 if (opt.value().id == tourop.value().user_id)
                 {
                     if (!storage.insertTourOperatorVacation(id, vac_id))
                     {
                         res.status_code = 418;
                         res.status_description = "I'm a teapot";
                     }
                 }
                 else
                 {
                     res.status_code = 403;
                     res.status_description = "Forbidden";
                 }
             }
             else
             {
                 res.status_code = 401;
                 res.status_description = "Unauthorized";
             }

         }


     });

     http_server.post("/vacations", [&](HttpRequest & req, HttpResponse & res)
     {

         string city = req.query["city"].toStdString();
         int duration = req.query["duration"].toInt();
         string hotel_name = req.query["hotel_name"].toStdString();
         vacation vac;
         vac.city = city;
         vac.duration = duration;
         vac.hotel_name = hotel_name;
         QJsonObject obj;
         int id = storage.insertVacation(vac);
         obj.insert("id", id);
         res.body = jsonObjectToString(obj);
     });

     http_server.post("/vacations/:id", [&](HttpRequest & req, HttpResponse & res)
     {
         int id = req.params["id"].toInt();
         string city = req.query["city"].toStdString();
         int duration = req.query["duration"].toInt();
         string hotel_name = req.query["hotel_name"].toStdString();
         vacation vac;
         vac.city = city;
         vac.duration = duration;
         vac.hotel_name = hotel_name;
         vac.id = id;
         if (!storage.updateVacation(vac))
         {
                 res.status_code = 404;
                 res.status_description = "Not Found";
         }
     });

     http_server.delete_("/vacations/:id", [&](HttpRequest & req, HttpResponse & res)
     {
         int id = req.params["id"].toInt();
         int tourop_id  = req.query["id"].toInt();
         optional<User> opt = userAuth(req, &storage);
         optional<tour_operator> tourop = storage.getTourOperatorById(tourop_id);
         if (!tourop)
         {
             res.status_code = 404;
             res.status_description = "Not found";
          }
         else
         {
             if (opt)
             {
                 if (opt.value().id == tourop.value().user_id)
                 {
                     if (!storage.removeTourOperatorVacation(tourop_id, id))
                     {
                         res.status_code = 418;
                         res.status_description = "I'm a teapot";
                     }
                 }
                 else
                 {
                     res.status_code = 403;
                     res.status_description = "Forbidden";
                 }
             }
             else
             {
                 res.status_code = 401;
                 res.status_description = "Unauthorized";
             }
         }

     });
     

//     http_server.put("/notes", [&](HttpRequest & req, HttpResponse & res){  // PUT /notes
//        Note updated_note = jsonToNote(stringToJsonDocument(req.body).object());
//        //
//     });
//     http_server.delete_("/notes/:id", [&](HttpRequest & req, HttpResponse & res){  // DELETE /notes/3
//        int delete_id = req.params["id"].toInt();
//        //
//     });


     const int port = 9999;
     if (http_server.start(port))
         qDebug() << "started at port " << port;
     else
         qDebug() << "can't start at port " << port;

     return a.exec();

}


optional<User> userAuth(HttpRequest & req, SqliteStorage* storage)
{
    QString username;
    QString password;
    QString st = QString::fromStdString(req.headers["Authorization"].toStdString());
    if (st.startsWith("Basic "))
    {
        QStringList list;
        list = st.split(" ");
        st = list[1];
        QByteArray arr;
        arr = arr.fromBase64(QByteArray::fromStdString(st.toStdString()));
        st = st.fromStdString(arr.toStdString());
        list = st.split(":");
        if (list.size() < 2)
        {
            return nullopt;
        }
        username = list[0];
        password = list[1];
    }
    //storage.getUserAuth();
    if (storage->getUserAuth(username.toStdString(), password.toStdString()))
    {
        return storage->getUserAuth(username.toStdString(), password.toStdString());
    }
    else
    {
        return nullopt;
    }
}
