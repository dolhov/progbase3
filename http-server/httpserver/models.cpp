#include "models.h"

tour_operator jsonToTourOperator(const QJsonObject & jobj)
{
   tour_operator s;
   s.id = jobj.value("id").toInt();
   s.name = jobj.value("name").toString().toStdString();
   s.city= jobj.value("city").toString().toStdString();
   s.vacation = jobj.value("vacation").toString().toStdString();
   s.cost = jobj.value("cost").toInt();
   s.currency =  jobj.value("currency").toString().toStdString();
   return s;
}

QVector<tour_operator> jsonToTourOperators(const QJsonArray & arr)
{
    QJsonValue val;
    QJsonObject obj;
    QVector<tour_operator> vec;
    for (int i = 0; i < arr.size(); i++)
    {
        tour_operator tour_op;
        val = arr[i];
        obj = val.toObject();
        tour_op.id = obj.value("id").toInt();
        tour_op.name = obj.value("name").toString().toStdString();
        tour_op.city = obj.value("city").toString().toStdString();
        tour_op.vacation = obj.value("vacation").toString().toStdString();
        tour_op.cost = obj.value("cost").toInt();
        tour_op.currency = obj.value("currency").toString().toStdString();
        vec.push_back(tour_op);
    }
    return vec;
}

QJsonObject TourOperatorToJson(const tour_operator & st)
{
   QJsonObject j;
   j.insert("id", st.id);
   j.insert("name", QString::fromStdString(st.name));
   j.insert("city",  QString::fromStdString(st.city));
   j.insert("vacation",  QString::fromStdString(st.vacation));
   j.insert("cost", st.cost);
   j.insert("currency",  QString::fromStdString(st.currency));
   return j;
}

QString jsonToString(const QJsonObject & jobj)
{
   QJsonDocument doc;
   doc.setObject(jobj);
   return doc.toJson();
}



QJsonArray TourOperatorsToJson(const QVector<tour_operator> & tour_ops)
{
    QJsonArray jarr;
    for (const tour_operator  & tourop: tour_ops)
        jarr.push_back(TourOperatorToJson(tourop));
    return jarr;
}

QJsonObject VacationToJson(const vacation & st)
{
    QJsonObject j;
    j.insert("id", st.id);
    j.insert("city",  QString::fromStdString(st.city));
    j.insert("duration", st.duration);
    j.insert("hotel_name",  QString::fromStdString(st.hotel_name));
    return j;
}

QJsonArray VacationsToJson(const QVector<vacation> & vacs)
{
    QJsonArray jarr;
    for (const vacation  & vc: vacs)
      jarr.push_back(VacationToJson(vc));
    return jarr;
}

