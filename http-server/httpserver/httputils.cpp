//#include "httputils.h"

//QString httpGet(QNetworkAccessManager & manager, const QString & url)
//{
//   QNetworkRequest request{url};
//   NetworkWaiter waiter{manager};
//   QNetworkReply *reply = manager.get(request);
//   waiter.wait();
//   QString replyBodyString = QString::fromUtf8(reply->readAll());
//   QNetworkReply::NetworkError error = reply->error();
//   reply->deleteLater();
//   if (error != QNetworkReply::NetworkError::NoError)
//       throw NetworkException(error, replyBodyString);
//   return replyBodyString;
//}


//QString httpPost(
//    QNetworkAccessManager & manager, const QString & url,
//    const QString & content_type, const QString & body)
//{
//   QNetworkRequest request{url};
//   request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader, content_type);
//   NetworkWaiter waiter{manager};
//   QNetworkReply *reply = manager.post(request, body.toUtf8());
//   waiter.wait();
//   QString replyBodyString = QString::fromUtf8(reply->readAll());
//   QNetworkReply::NetworkError error = reply->error();
//   reply->deleteLater();
//   if (error != QNetworkReply::NetworkError::NoError)
//       throw NetworkException(error, replyBodyString);
//   return replyBodyString;
//}


//QString httpPut(
//    QNetworkAccessManager & manager, const QString & url,
//    const QString & content_type, const QString & body)
//{
//   QNetworkRequest request{url};
//   request.setHeader(
//       QNetworkRequest::KnownHeaders::ContentTypeHeader,
//       content_type);
//   NetworkWaiter waiter{manager};
//   QNetworkReply *reply = manager.put(request, body.toUtf8());
//   waiter.wait();
//   QString replyBodyString = QString::fromUtf8(reply->readAll());
//   QNetworkReply::NetworkError error = reply->error();
//   reply->deleteLater();
//   if (error != QNetworkReply::NetworkError::NoError)
//       throw NetworkException(error, replyBodyString);
//   return replyBodyString;
//}

//QString httpDelete(
//    QNetworkAccessManager & manager,
//    const QString & url)
//{
//   QNetworkRequest request{url};
//   NetworkWaiter waiter{manager};
//   QNetworkReply *reply = manager.deleteResource(request);
//   waiter.wait();
//   QString replyBodyString = QString::fromUtf8(reply->readAll());
//   QNetworkReply::NetworkError error = reply->error();
//   reply->deleteLater();
//   if (error != QNetworkReply::NetworkError::NoError)
//       throw NetworkException(error, replyBodyString);
//   return replyBodyString;
//}
