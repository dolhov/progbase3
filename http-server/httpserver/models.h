#ifndef MODELS_H
#define MODELS_H
#include "jsonutils.h"
#include "tour_operators.h"
#include <QVector>
#include "vacation.h"
tour_operator jsonToTourOperator(const QJsonObject & jobj);
QVector<tour_operator> jsonToTourOperators(const QJsonArray & arr);

QJsonObject TourOperatorToJson(const tour_operator & st);
QJsonArray TourOperatorsToJson(const QVector<tour_operator> & tour_ops);

QJsonObject VacationToJson(const vacation & st);
QJsonArray VacationsToJson(const QVector<vacation> & vacs);


#endif // MODELS_H
