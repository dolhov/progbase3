/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_Storage;
    QAction *actionOpen_Storage;
    QAction *actionNew_Storage_2;
    QAction *actionOpen_Storage_2;
    QAction *actionLogout;
    QAction *actionAdd;
    QAction *actionEdit;
    QAction *actionRemove;
    QAction *actionChange_avatar;
    QAction *actionAdd_vacation;
    QWidget *centralWidget;
    QListWidget *listWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QLabel *label_7;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_4;
    QLabel *label_6;
    QLabel *label_10;
    QLabel *label_9;
    QLabel *label_8;
    QLabel *label_3;
    QLabel *label_11;
    QLabel *label_12;
    QPushButton *pushButton_5;
    QPushButton *pushButton_4;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_16;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *lineEdit;
    QLabel *label_14;
    QComboBox *comboBox;
    QPushButton *pushButton_6;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label_17;
    QPushButton *pushButton_3;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuUser;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(685, 387);
        actionNew_Storage = new QAction(MainWindow);
        actionNew_Storage->setObjectName(QStringLiteral("actionNew_Storage"));
        actionOpen_Storage = new QAction(MainWindow);
        actionOpen_Storage->setObjectName(QStringLiteral("actionOpen_Storage"));
        actionNew_Storage_2 = new QAction(MainWindow);
        actionNew_Storage_2->setObjectName(QStringLiteral("actionNew_Storage_2"));
        actionNew_Storage_2->setCheckable(false);
        actionNew_Storage_2->setEnabled(false);
        actionNew_Storage_2->setIconVisibleInMenu(true);
        actionOpen_Storage_2 = new QAction(MainWindow);
        actionOpen_Storage_2->setObjectName(QStringLiteral("actionOpen_Storage_2"));
        actionLogout = new QAction(MainWindow);
        actionLogout->setObjectName(QStringLiteral("actionLogout"));
        actionAdd = new QAction(MainWindow);
        actionAdd->setObjectName(QStringLiteral("actionAdd"));
        actionEdit = new QAction(MainWindow);
        actionEdit->setObjectName(QStringLiteral("actionEdit"));
        actionRemove = new QAction(MainWindow);
        actionRemove->setObjectName(QStringLiteral("actionRemove"));
        actionChange_avatar = new QAction(MainWindow);
        actionChange_avatar->setObjectName(QStringLiteral("actionChange_avatar"));
        actionAdd_vacation = new QAction(MainWindow);
        actionAdd_vacation->setObjectName(QStringLiteral("actionAdd_vacation"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 40, 221, 181));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(240, 40, 211, 187));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 11, 0, 1, 1);

        label_7 = new QLabel(gridLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout->addWidget(label_7, 13, 0, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 3, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 6, 0, 1, 1);

        label_4 = new QLabel(gridLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 6, 1, 1, 3);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 12, 0, 1, 1);

        label_10 = new QLabel(gridLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout->addWidget(label_10, 13, 1, 1, 3);

        label_9 = new QLabel(gridLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout->addWidget(label_9, 12, 1, 1, 3);

        label_8 = new QLabel(gridLayoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout->addWidget(label_8, 11, 1, 1, 3);

        label_3 = new QLabel(gridLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 3, 1, 1, 3);

        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(50, 10, 151, 20));
        QFont font;
        font.setFamily(QStringLiteral("Ubuntu"));
        font.setPointSize(14);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        label_11->setFont(font);
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(300, 10, 91, 17));
        QFont font1;
        font1.setPointSize(14);
        label_12->setFont(font1);
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(10, 230, 61, 25));
        pushButton_4 = new QPushButton(centralWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(160, 230, 71, 25));
        horizontalLayoutWidget_2 = new QWidget(centralWidget);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(80, 220, 75, 41));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_13 = new QLabel(horizontalLayoutWidget_2);
        label_13->setObjectName(QStringLiteral("label_13"));
        QFont font2;
        font2.setPointSize(10);
        label_13->setFont(font2);

        horizontalLayout_2->addWidget(label_13);

        label_15 = new QLabel(horizontalLayoutWidget_2);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_2->addWidget(label_15);

        label_16 = new QLabel(horizontalLayoutWidget_2);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_2->addWidget(label_16);

        horizontalLayoutWidget_3 = new QWidget(centralWidget);
        horizontalLayoutWidget_3->setObjectName(QStringLiteral("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 270, 411, 61));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        lineEdit = new QLineEdit(horizontalLayoutWidget_3);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_3->addWidget(lineEdit);

        label_14 = new QLabel(horizontalLayoutWidget_3);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_3->addWidget(label_14);

        comboBox = new QComboBox(horizontalLayoutWidget_3);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        horizontalLayout_3->addWidget(comboBox);

        pushButton_6 = new QPushButton(horizontalLayoutWidget_3);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        horizontalLayout_3->addWidget(pushButton_6);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(240, 250, 171, 21));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(510, 210, 101, 25));
        label_17 = new QLabel(centralWidget);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setGeometry(QRect(470, 50, 181, 131));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setEnabled(true);
        pushButton_3->setGeometry(QRect(510, 260, 89, 25));
        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 685, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuUser = new QMenu(menuBar);
        menuUser->setObjectName(QStringLiteral("menuUser"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuUser->menuAction());
        menuFile->addAction(actionNew_Storage_2);
        menuFile->addAction(actionOpen_Storage_2);
        menuFile->addAction(actionLogout);
        menuFile->addAction(actionAdd);
        menuFile->addAction(actionEdit);
        menuFile->addAction(actionRemove);
        menuUser->addAction(actionChange_avatar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionNew_Storage->setText(QApplication::translate("MainWindow", "New Storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew_Storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen_Storage->setText(QApplication::translate("MainWindow", "Open Storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen_Storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionNew_Storage_2->setText(QApplication::translate("MainWindow", "New Storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew_Storage_2->setShortcut(QApplication::translate("MainWindow", "Ctrl+A", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen_Storage_2->setText(QApplication::translate("MainWindow", "Login", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen_Storage_2->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionLogout->setText(QApplication::translate("MainWindow", "Logout", Q_NULLPTR));
        actionAdd->setText(QApplication::translate("MainWindow", "Add", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionAdd->setShortcut(QApplication::translate("MainWindow", "Ctrl+A", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionEdit->setText(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionEdit->setShortcut(QApplication::translate("MainWindow", "Ctrl+E", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionRemove->setText(QApplication::translate("MainWindow", "Remove", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionRemove->setShortcut(QApplication::translate("MainWindow", "Ctrl+R", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionChange_avatar->setText(QApplication::translate("MainWindow", "Change avatar", Q_NULLPTR));
        actionAdd_vacation->setText(QApplication::translate("MainWindow", "Add vacation", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Vacation", Q_NULLPTR));
        label_7->setText(QApplication::translate("MainWindow", "Currency", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Name:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "City:", Q_NULLPTR));
        label_4->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "Cost", Q_NULLPTR));
        label_10->setText(QString());
        label_9->setText(QString());
        label_8->setText(QString());
        label_3->setText(QString());
        label_11->setText(QApplication::translate("MainWindow", "Tour operators", Q_NULLPTR));
        label_12->setText(QString());
        pushButton_5->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Page", Q_NULLPTR));
        label_15->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        label_16->setText(QApplication::translate("MainWindow", "/  1", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "Search by:", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Name", Q_NULLPTR)
         << QApplication::translate("MainWindow", "City", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Vacation", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Cost", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Currency", Q_NULLPTR)
        );
        pushButton_6->setText(QApplication::translate("MainWindow", "Search", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MainWindow", "Back to tour operators", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MainWindow", "Change avatar", Q_NULLPTR));
        label_17->setText(QString());
        pushButton_3->setText(QApplication::translate("MainWindow", "Display", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
        menuUser->setTitle(QApplication::translate("MainWindow", "User", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
