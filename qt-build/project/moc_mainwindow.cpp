/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../qt-corsework client-server/project/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[29];
    char stringdata0[439];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 20), // "actionOpen_Storage_2"
QT_MOC_LITERAL(4, 55, 19), // "actionNew_Storage_2"
QT_MOC_LITERAL(5, 75, 12), // "actionLogout"
QT_MOC_LITERAL(6, 88, 9), // "actionAdd"
QT_MOC_LITERAL(7, 98, 10), // "actionEdit"
QT_MOC_LITERAL(8, 109, 12), // "actionRemove"
QT_MOC_LITERAL(9, 122, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(10, 148, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(11, 165, 4), // "item"
QT_MOC_LITERAL(12, 170, 5), // "logIn"
QT_MOC_LITERAL(13, 176, 20), // "displayTourOperators"
QT_MOC_LITERAL(14, 197, 26), // "std::vector<tour_operator>"
QT_MOC_LITERAL(15, 224, 7), // "tourops"
QT_MOC_LITERAL(16, 232, 23), // "getTourOperatorsForPage"
QT_MOC_LITERAL(17, 256, 4), // "page"
QT_MOC_LITERAL(18, 261, 10), // "Attributes"
QT_MOC_LITERAL(19, 272, 3), // "atr"
QT_MOC_LITERAL(20, 276, 11), // "std::string"
QT_MOC_LITERAL(21, 288, 9), // "parameter"
QT_MOC_LITERAL(22, 298, 12), // "userListSize"
QT_MOC_LITERAL(23, 311, 7), // "user_id"
QT_MOC_LITERAL(24, 319, 23), // "on_pushButton_4_clicked"
QT_MOC_LITERAL(25, 343, 23), // "on_pushButton_5_clicked"
QT_MOC_LITERAL(26, 367, 23), // "on_pushButton_6_clicked"
QT_MOC_LITERAL(27, 391, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(28, 415, 23) // "on_pushButton_3_clicked"

    },
    "MainWindow\0on_pushButton_clicked\0\0"
    "actionOpen_Storage_2\0actionNew_Storage_2\0"
    "actionLogout\0actionAdd\0actionEdit\0"
    "actionRemove\0on_listWidget_itemClicked\0"
    "QListWidgetItem*\0item\0logIn\0"
    "displayTourOperators\0std::vector<tour_operator>\0"
    "tourops\0getTourOperatorsForPage\0page\0"
    "Attributes\0atr\0std::string\0parameter\0"
    "userListSize\0user_id\0on_pushButton_4_clicked\0"
    "on_pushButton_5_clicked\0on_pushButton_6_clicked\0"
    "on_pushButton_2_clicked\0on_pushButton_3_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x08 /* Private */,
       3,    0,  120,    2, 0x08 /* Private */,
       4,    0,  121,    2, 0x08 /* Private */,
       5,    0,  122,    2, 0x08 /* Private */,
       6,    0,  123,    2, 0x08 /* Private */,
       7,    0,  124,    2, 0x08 /* Private */,
       8,    0,  125,    2, 0x08 /* Private */,
       9,    1,  126,    2, 0x08 /* Private */,
      12,    0,  129,    2, 0x08 /* Private */,
      13,    1,  130,    2, 0x08 /* Private */,
      16,    3,  133,    2, 0x08 /* Private */,
      16,    2,  140,    2, 0x28 /* Private | MethodCloned */,
      16,    1,  145,    2, 0x28 /* Private | MethodCloned */,
      22,    3,  148,    2, 0x08 /* Private */,
      22,    2,  155,    2, 0x28 /* Private | MethodCloned */,
      22,    1,  160,    2, 0x28 /* Private | MethodCloned */,
      24,    0,  163,    2, 0x08 /* Private */,
      25,    0,  164,    2, 0x08 /* Private */,
      26,    0,  165,    2, 0x08 /* Private */,
      27,    0,  166,    2, 0x08 /* Private */,
      28,    0,  167,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Bool,
    QMetaType::Void, 0x80000000 | 14,   15,
    0x80000000 | 14, QMetaType::UInt, 0x80000000 | 18, 0x80000000 | 20,   17,   19,   21,
    0x80000000 | 14, QMetaType::UInt, 0x80000000 | 18,   17,   19,
    0x80000000 | 14, QMetaType::UInt,   17,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 18, 0x80000000 | 20,   23,   19,   21,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 18,   23,   19,
    QMetaType::Void, QMetaType::Int,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_pushButton_clicked(); break;
        case 1: _t->actionOpen_Storage_2(); break;
        case 2: _t->actionNew_Storage_2(); break;
        case 3: _t->actionLogout(); break;
        case 4: _t->actionAdd(); break;
        case 5: _t->actionEdit(); break;
        case 6: _t->actionRemove(); break;
        case 7: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 8: { bool _r = _t->logIn();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: _t->displayTourOperators((*reinterpret_cast< std::vector<tour_operator>(*)>(_a[1]))); break;
        case 10: { std::vector<tour_operator> _r = _t->getTourOperatorsForPage((*reinterpret_cast< uint(*)>(_a[1])),(*reinterpret_cast< Attributes(*)>(_a[2])),(*reinterpret_cast< std::string(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< std::vector<tour_operator>*>(_a[0]) = std::move(_r); }  break;
        case 11: { std::vector<tour_operator> _r = _t->getTourOperatorsForPage((*reinterpret_cast< uint(*)>(_a[1])),(*reinterpret_cast< Attributes(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< std::vector<tour_operator>*>(_a[0]) = std::move(_r); }  break;
        case 12: { std::vector<tour_operator> _r = _t->getTourOperatorsForPage((*reinterpret_cast< uint(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< std::vector<tour_operator>*>(_a[0]) = std::move(_r); }  break;
        case 13: _t->userListSize((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< Attributes(*)>(_a[2])),(*reinterpret_cast< std::string(*)>(_a[3]))); break;
        case 14: _t->userListSize((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< Attributes(*)>(_a[2]))); break;
        case 15: _t->userListSize((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->on_pushButton_4_clicked(); break;
        case 17: _t->on_pushButton_5_clicked(); break;
        case 18: _t->on_pushButton_6_clicked(); break;
        case 19: _t->on_pushButton_2_clicked(); break;
        case 20: _t->on_pushButton_3_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 21;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
