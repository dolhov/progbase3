#include "auntefication.h"
#include "ui_auntefication.h"

auntefication::auntefication(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::auntefication)
{
    ui->setupUi(this);
}

auntefication::~auntefication()
{
    delete ui;
}


void auntefication::closeSocket(QTcpSocket & client_socket)
{
   if (client_socket.state() == QTcpSocket::SocketState::ConnectedState)
   {
       qDebug() << "Start disconnecting from server...";
       client_socket.disconnect();
       if (!client_socket.waitForDisconnected(1000))
       {
           qDebug() << "Disconnected.";
       }
       else
       {
           qDebug() << "Disconnecting timeout (1000).";
       }
   }
   else
   {
       qDebug() << "Already disconnected from server.";
   }
}


int auntefication::user_id()
{
    return this->userid;
}

void auntefication::on_buttonBox_accepted()
{
    int port = 9999;
    QTcpSocket socket;
    socket.connectToHost(QHostAddress::LocalHost, port);
    if (!socket.waitForConnected(3000))
    {
        qDebug() << "Connection timeout to port: " << port;
        closeSocket(socket);
        QMessageBox::information(
                    this,
                    "Information",
                    "Can't connect to server");
    }
    else
    {
        QDomDocument xmlRequest;
        QDomElement root = xmlRequest.createElement("requests");
        xmlRequest.appendChild(root);
        QDomElement user = xmlRequest.createElement("user");
        root.setAttribute("request", "login");
        user.setAttribute("username",ui->lineEdit->text());
        user.setAttribute("password", ui->lineEdit_2->text());
        root.appendChild(user);
        socket.write(xmlRequest.toByteArray());
        if (!socket.waitForBytesWritten(3000)){
            qDebug() << "Fail to write";
        }
        if (!socket.waitForReadyRead(1000))
        {
            qDebug() << "no response from server, timeout (1000)";
            return;
        }
        else
        {
            QByteArray response_data = socket.readAll();
            QDomDocument response;
            response.setContent(response_data);
            qDebug() << QString::fromStdString(response_data.toStdString());
            QDomElement el = response.documentElement();
            if (el.tagName() == "response")
            {
                QDomNode domnode = response.documentElement();
                domnode = domnode.firstChild();
                if (domnode.isElement())
                {
                    QDomElement domElement = domnode.toElement();
                    if (!domElement.isNull())
                    {
                        if (domElement.tagName() == "user")
                        {
                            int user_id = domElement.attribute("user_id", "").toInt();
                            if (user_id == -1)
                            {
                                QMessageBox::information(
                                            this,
                                            "Information",
                                            "Invalid username or password");
                            }
                            else
                            {
                                QMessageBox::information(
                                            this,
                                            "Information",
                                            "Log in succefull");
                                this->close();
                            }
                            this->userid = user_id;
                        }
                    }

                }
            }
            closeSocket(socket);
        }
    }
}

//signin
void auntefication::on_pushButton_clicked()
{
    SiginIn dialog(this);
    dialog.setStorage(this->stor_);
    int status = dialog.exec();
    std::vector<std::string> st = dialog.namePassword();
    qDebug() << "status: " << status;

}


std::vector<std::string> auntefication::signIn()
{
    return this->newNamePassword;
}


void auntefication::setStorage(Storage * stor)
{
    this->stor_ = stor;
}
