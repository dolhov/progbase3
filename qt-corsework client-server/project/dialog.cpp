#include "dialog.h"
#include "ui_dialog.h"

AddVac::AddVac(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

AddVac::~AddVac()
{
    delete ui;
}

void AddVac::on_buttonBox_accepted()
{
    vacation vac;
    vac.city = ui->lineEdit->text().toStdString();
    vac.duration = ui->spinBox->value();
    vac.hotel_name = ui->lineEdit_2->text().toStdString();
    this->vac = vac;
}

 vacation AddVac::addedVacation()
 {
    return vac;
 }
