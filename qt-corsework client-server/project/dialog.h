#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "vacation.h"

namespace Ui {
class Dialog;
}

class AddVac : public QDialog
{
    Q_OBJECT

public:
    explicit AddVac(QWidget *parent = 0);
    ~AddVac();
    vacation addedVacation();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Dialog *ui;
    vacation vac;
};

#endif // DIALOG_H
