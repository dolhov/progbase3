#include "siginin.h"
#include "ui_siginin.h"



void SiginIn::closeSocket(QTcpSocket & client_socket)
{
   if (client_socket.state() == QTcpSocket::SocketState::ConnectedState)
   {
       qDebug() << "Start disconnecting from server...";
       client_socket.disconnect();
       if (!client_socket.waitForDisconnected(1000))
       {
           qDebug() << "Disconnected.";
       }
       else
       {
           qDebug() << "Disconnecting timeout (1000).";
       }
   }
   else
   {
       qDebug() << "Already disconnected from server.";
   }
}


SiginIn::SiginIn(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SiginIn)
{
    ui->setupUi(this);
}

SiginIn::~SiginIn()
{
    delete ui;
}




std::vector<std::string> SiginIn::namePassword()
{
    return this->name_password;
}

void SiginIn::on_pushButton_2_clicked()
{
    int port = 9999;
      QTcpSocket socket;
      socket.connectToHost(QHostAddress::LocalHost, port);
      if (!socket.waitForConnected(3000))
      {
          qDebug() << "Connection timeout to port: " << port;
          closeSocket(socket);
      }
      else
      {
          QDomDocument xmlRequest;
          QDomElement root = xmlRequest.createElement("requests");
          xmlRequest.appendChild(root);
          QDomElement user = xmlRequest.createElement("user");
          root.setAttribute("request", "signin");
          if (ui->lineEdit_2->text() != ui->lineEdit_3->text())
          {
              QMessageBox::information(
                  this,
                  "Information",
                  "Passwords are not same. Re-enter passwords");
          }
          else
          {
              user.setAttribute("username",ui->lineEdit->text());
              user.setAttribute("password", ui->lineEdit_2->text());
              root.appendChild(user);
              socket.write(xmlRequest.toByteArray());
              if (!socket.waitForBytesWritten(3000)){
                  qDebug() << "Fail to write";
              }
              if (!socket.waitForReadyRead(1000))
                 {
                     qDebug() << "no response from server, timeout (1000)";
                     return;
              }
              else
              {
                  QByteArray response_data = socket.readAll();
                  QString response_string = QString::fromStdString(response_data.toStdString());
                  QMessageBox::information(
                              this,
                              "Information",
                              response_string);
              }
          }
          closeSocket(socket);
      }

      //    if (!st->isUniqueUser(ui->lineEdit->text().toStdString()))
//    {
//        QMessageBox::information(
//            this,
//            "Information",
//            "This username is already in use");
//    }

//    else if (ui->lineEdit_2->text().size() < 6)
//    {
//        QMessageBox::information(
//            this,
//            "Information",
//            "Password should be at least 6 charachters long");
//    }
//    else
//    {

//         qDebug() << ui->lineEdit->text();
//         this->st->insertUser(ui->lineEdit->text().toStdString(), ui->lineEdit_2->text().toStdString());
//         QMessageBox::information(
//                this,
//                "Information",
//                "Succesfull signin");

//        this->accept();
//    }
}

void SiginIn::on_pushButton_clicked()
{
    this->close();
}

 void SiginIn::setStorage(Storage * stor)
 {
     this->st = stor;
 }
