#ifndef AUNTEFICATION_H
#define AUNTEFICATION_H

#include <QDialog>
#include <vector>
#include <string>
#include <QDebug>
#include <QtXml>
#include <QTcpSocket>
#include <QHostAddress>
#include "siginin.h"
#include "sqlitestorage.h"

namespace Ui {
class auntefication;
}

class auntefication : public QDialog
{
    Q_OBJECT
    std::vector<std::string> newNamePassword;
    Storage * stor_;

public:
    explicit auntefication(QWidget *parent = 0);
    ~auntefication();
    void setStorage(Storage * stor_);
    int user_id();
    std::vector<std::string> signIn();
private slots:
    void on_buttonBox_accepted();
    void closeSocket(QTcpSocket & client_socket);
    void on_pushButton_clicked();

private:
    Ui::auntefication *ui;
    int userid = -1;
};

#endif // AUNTEFICATION_H
