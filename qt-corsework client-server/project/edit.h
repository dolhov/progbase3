#ifndef EDIT_H
#define EDIT_H

#include <QDialog>
#include "tour_operators.h"
#include "vacation.h"
#include "sqlitestorage.h"
#include <QMessageBox>
#include <QListWidgetItem>
#include <QDomDocument>
#include "vacations_list.h"
#include "dialog.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QAction>

#include <QTcpSocket>
#include <QHostAddress>
#include <QtXml>

namespace Ui {
class Edit;
}

class Edit : public QDialog
{
    Q_OBJECT
    Storage * st;
    vector<vacation> stor_;
    int tourop_id;
public:
    explicit Edit(QWidget *parent = 0);
    ~Edit();

    void DisplayVacations();
    tour_operator data();
    void SetStorage(Storage * st);
    void SetTourOperatorId(int id);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_listWidget_2_itemClicked(QListWidgetItem *item);

    void on_pushButton_3_clicked();

private:
    Ui::Edit *ui;
};

#endif // EDIT_H
