#ifndef SIGININ_H
#define SIGININ_H

#include <QDialog>
#include <QMessageBox>
#include <QtXml>
#include <QTcpSocket>
#include <QHostAddress>
#include "sqlitestorage.h"


namespace Ui {
class SiginIn;
}

class SiginIn : public QDialog
{
    Q_OBJECT
    Storage * st;
     std::vector<std::string> name_password;
     std::vector<std::string> userNames;

public:
    explicit SiginIn(QWidget *parent = 0);
    ~SiginIn();

     std::vector<std::string> namePassword();
     void setStorage(Storage * stor);

private slots:

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::SiginIn *ui;
    void closeSocket(QTcpSocket & client_socket);
};

#endif // SIGININ_H
