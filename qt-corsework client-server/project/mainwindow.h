#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QPixmap>
#include <QImageReader>
#include "FileStorage.h"
#include "sqlitestorage.h"
#include "addtouroperator.h"
#include "edit.h"
#include "auntefication.h"

#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QAction>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_pushButton_clicked();
    void actionOpen_Storage_2();
    void actionNew_Storage_2();
    void actionLogout();
    void actionAdd();
    void actionEdit();
    void actionRemove();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    bool logIn();


    void displayTourOperators(std::vector<tour_operator> tourops);
    std::vector<tour_operator> getTourOperatorsForPage(unsigned int page, Attributes atr = Attributes::EMPTY, std::string parameter = "");
    void userListSize(int user_id, Attributes atr = Attributes::EMPTY, std::string parameter = "");
    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    void displayPicture();
    void closeSocket(QTcpSocket & client_socket);
    Ui::MainWindow *ui;
    Storage * storage_ = nullptr;
    vector<tour_operator> saved_tour_operators;
    int user_loged_id = -1;
    unsigned int current_page = 1;
    unsigned int max_page = 1;
    enum Attributes current_search_atr = Attributes::EMPTY;
    std::string current_search_parametr = "";
};

#endif // MAINWINDOW_H
