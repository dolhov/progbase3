#include "edit.h"
#include "ui_edit.h"

Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
}

Edit::~Edit()
{
    delete ui;
}


void closeSocket(QTcpSocket & client_socket)
{
   if (client_socket.state() == QTcpSocket::SocketState::ConnectedState)
   {
       qDebug() << "Start disconnecting from server...";
       client_socket.disconnect();
       if (!client_socket.waitForDisconnected(1000))
       {
           qDebug() << "Disconnected.";
       }
       else
       {
           qDebug() << "Disconnecting timeout (1000).";
       }
   }
   else
   {
       qDebug() << "Already disconnected from server.";
   }
}

void Edit::SetStorage(Storage * st)
{
    this->st = st;
}

void Edit::SetTourOperatorId(int id)
{
    this->tourop_id = id;
}

tour_operator Edit::data()
{
    tour_operator tourop;
    tourop.name = ui->lineEdit->text().toStdString();
    tourop.city = ui->lineEdit_2->text().toStdString();
    tourop.vacation = ui->lineEdit_3->text().toStdString();
    tourop.cost = ui->spinBox->value();
    tourop.currency = ui->lineEdit_4->text().toStdString();
    return tourop;
}


void Edit::DisplayVacations()
{
    ui->listWidget_2->clear();
    int port = 9999;
    QTcpSocket socket;
    socket.connectToHost(QHostAddress::LocalHost, port);
    if (!socket.waitForConnected(3000))
    {
        qDebug() << "Connection timeout to port: " << port;
        closeSocket(socket);
    }
    else
    {
        std::vector<vacation> vac;
        QDomDocument xmlRequest;
        QDomElement root = xmlRequest.createElement("requests");
        root.setAttribute("request", "Vacations_tourop");
        root.setAttribute("tourop_id", this->tourop_id);
        xmlRequest.appendChild(root);
        socket.write(xmlRequest.toByteArray());
        if (!socket.waitForBytesWritten(3000)){
            qDebug() << "Fail to write";
        }
        if (!socket.waitForReadyRead(10000))
        {
            qDebug() << "no response from server, timeout (10000)";
            return;
        }
        else
        {
            QByteArray response_data = socket.readAll();
            QDomDocument response_doc;
            response_doc.setContent(response_data);
            qDebug() << "Answer: " << response_doc.toString();
            QDomElement el = response_doc.documentElement();
            if (el.tagName() == "vacations")
            {
                QDomNode domnode = response_doc.documentElement();
                domnode = domnode.firstChild();
                vector<vacation> list;
                vacation op;
                for (;domnode.isElement() && !domnode.isNull();)
                {
                    QDomElement domElement = domnode.toElement();
                    if (domElement.tagName() == "vacation")
                    {
                        qDebug() << "Tour operator added";
                        op.id = domElement.attribute("id","").toInt();
                        op.city = domElement.attribute("city","").toStdString();
                        op.duration = domElement.attribute("duration","").toInt();
                        op.hotel_name = domElement.attribute("hotel_name","").toStdString();
                        list.push_back(op);
                    }
                    domnode = domnode.nextSibling();
                }
                stor_ = list;
                vac = list;
                closeSocket(socket);
                //                return list;
                //            QMessageBox::information(
                //                        this,
                //                        "Information",
                //                        response_string);
                //        }
                /*= this->st->getAllTourOperatorsVacations(this->tourop_id);*/
                for (vacation & vc : vac)
                {
                    QString text = "id";
                    text += QString::fromStdString(std::to_string(vc.id));
                    text +=" ";
                    text += QString::fromStdString(vc.city);
                    text += ";";

                    QListWidgetItem * new_item = new QListWidgetItem(text);
                    QVariant var = QVariant::fromValue(vc.id);
                    new_item->setData(Qt::UserRole, var);
                    ui->listWidget_2->addItem(new_item);
                }


            }
        }
    }
}

 //add vacation
void Edit::on_pushButton_clicked()
{
    vacations_list dialog(this);
    int port = 9999;
    QTcpSocket socket;
    socket.connectToHost(QHostAddress::LocalHost, port);
    std::vector<vacation> vac;
    if (!socket.waitForConnected(3000))
    {
        qDebug() << "Connection timeout to port: " << port;
        closeSocket(socket);
    }
    else
    {

        QDomDocument xmlRequest;
        QDomElement root = xmlRequest.createElement("requests");
        root.setAttribute("request", "Vacations");
        xmlRequest.appendChild(root);
        socket.write(xmlRequest.toByteArray());
        if (!socket.waitForBytesWritten(3000)){
            qDebug() << "Fail to write";
        }
        if (!socket.waitForReadyRead(10000))
        {
            qDebug() << "no response from server, timeout (10000)";
            return;
        }
        else
        {
            QByteArray response_data = socket.readAll();
            QDomDocument response_doc;
            response_doc.setContent(response_data);
            qDebug() << "Answer: " << response_doc.toString();
            QDomElement el = response_doc.documentElement();
            if (el.tagName() == "vacations")
            {
                QDomNode domnode = response_doc.documentElement();
                domnode = domnode.firstChild();
                vector<vacation> list;
                vacation op;
                for (;domnode.isElement() && !domnode.isNull();)
                {
                    QDomElement domElement = domnode.toElement();
                    if (domElement.tagName() == "vacation")
                    {
                        qDebug() << "Vacation added";
                        op.id = domElement.attribute("id","").toInt();
                        op.city = domElement.attribute("city","").toStdString();
                        op.duration = domElement.attribute("duration","").toInt();
                        op.hotel_name = domElement.attribute("hotel_name","").toStdString();
                        list.push_back(op);
                    }
                    domnode = domnode.nextSibling();
                }
                stor_ = list;
                vac = list;


            }
        }
    }

    dialog.setVacations(vac);
    int status = dialog.exec();
    qDebug() << "status: " << status;
    if (status == 1)
    {
        optional<int> vacation_id = dialog.vacationId();
        if (vacation_id)
        {
            socket.connectToHost(QHostAddress::LocalHost, port);
            QDomDocument xmlRequest;
            QDomElement root = xmlRequest.createElement("requests");
            root.setAttribute("request", "VacationInsert");
            root.setAttribute("tourop_id", this->tourop_id);
            root.setAttribute("vacation_id", vacation_id.value());
            xmlRequest.appendChild(root);
            socket.write(xmlRequest.toByteArray());
            if (!socket.waitForBytesWritten(3000)){
                qDebug() << "Fail to write";
            }
            if (!socket.waitForReadyRead(1000))
            {
                qDebug() << "no response from server, timeout (1000)";
                this->DisplayVacations();
                return;
            }
            else
            {
                QByteArray response_data = socket.readAll();
                QDomDocument response_doc;
                response_doc.setContent(response_data);
                qDebug() << "Answer: " << response_doc.toString();
            //this->st->insertTourOperatorVacation(this->tourop_id, vacation_id.value());
        }

    }
    closeSocket(socket);
}


//   int id = ui->spinBox_2->value();
//   bool checker = false;
//   std::vector<vacation> vacs = this->st->getAllVacations();
//   for (vacation vc : vacs)
//   {
//       if (vc.id == id)
//       {
//           st->insertTourOperatorVacation(this->tourop_id, vc.id);
//           checker = true;
//           break;
//       }
//   }
//   if (!checker)
//   {
//       QMessageBox::information(
//           this,
//           "Information",
//           "No vacation with such id");
//   }


   this->DisplayVacations();
}

//delete vacation
void Edit::on_pushButton_2_clicked()
{
    QList list = ui->listWidget_2->selectedItems();
    if (list.size() == 0)
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "Choose entity");
    }
    else
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "On delete",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {
            int port = 9999;
            QTcpSocket socket;
            socket.connectToHost(QHostAddress::LocalHost, port);
            if (!socket.waitForConnected(3000))
            {
                qDebug() << "Connection timeout to port: " << port;
                closeSocket(socket);
            }
            else
            {
                QListWidgetItem * item = list.at(list.size() - 1);
                QVariant var = item->data(Qt::UserRole);
                QDomDocument xmlRequest;
                QDomElement root = xmlRequest.createElement("requests");
                root.setAttribute("request", "deleteVacation");
                root.setAttribute("tourop_id", this->tourop_id);
                root.setAttribute("vacation_id", var.toInt());
                xmlRequest.appendChild(root);
                socket.write(xmlRequest.toByteArray());
                if (!socket.waitForBytesWritten(3000)){
                    qDebug() << "Fail to write";
                }
                if (!socket.waitForReadyRead(10000))
                {
                    qDebug() << "no response from server, timeout (10000)";
                    this->DisplayVacations();
                    return;
                }
                this->DisplayVacations();
                //                QListWidgetItem * item = list.at(list.size() - 1);
                //                QVariant var = item->data(Qt::UserRole);
                //                this->st->removeTourOperatorVacation(this->tourop_id ,var.toInt());
                //                QListWidgetItem * it = ui->listWidget_2->takeItem(ui->listWidget_2->currentRow());

                //                delete it;

            }
        }

    }
}

void Edit::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    int vac_id = var.toInt();
    vacation vac;
    for (int i = 0; i < stor_.size(); i++)
    {
        if (stor_[i].id == vac_id )
        {
            vac = stor_[i];
            break;
        }
    }
    ui->label_8->setText(QString::fromStdString(std::to_string(vac.duration)));
    ui->label_9->setText(QString::fromStdString(vac.hotel_name));
}

void Edit::on_pushButton_3_clicked()
{
    AddVac dialog(this);
    vacation vac;
    int status = dialog.exec();
    if(status == 1)
    {
        vac = dialog.addedVacation();
        int port = 9999;
        QTcpSocket socket;
        socket.connectToHost(QHostAddress::LocalHost, port);
        if (!socket.waitForConnected(3000))
        {
            qDebug() << "Connection timeout to port: " << port;
            closeSocket(socket);
        }
        else
        {
            QDomDocument xmlRequest;
            QDomElement root = xmlRequest.createElement("requests");
            root.setAttribute("request", "addVacation");
            xmlRequest.appendChild(root);
            QDomElement ent = xmlRequest.createElement("vacations");
            ent.setTagName("vacation");
            ent.setAttribute("city", QString::fromStdString(vac.city));
            ent.setAttribute("duration", vac.duration);
            ent.setAttribute("hotel_name", QString::fromStdString(vac.hotel_name));
            root.appendChild(ent);
            socket.write(xmlRequest.toByteArray());
            if (!socket.waitForBytesWritten(3000)){
                qDebug() << "Fail to write";
            }
            if (!socket.waitForReadyRead(10000))
            {
                qDebug() << "no response from server, timeout (10000)";
                return;
            }
            else
            {
                QByteArray response_data = socket.readAll();
                QString response_string = QString::fromStdString(response_data.toStdString());
                QMessageBox::information(
                            this,
                            "Information",
                            response_string);
            }

            closeSocket(socket);

        }
    }
}
