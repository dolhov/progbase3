#ifndef CLIENTTHREAD_H
#define CLIENTTHREAD_H

#include <QDebug>
#include <QThread>
#include <QVector>
#include <QTcpSocket>
#include "requestshandler.h"

class ClientThread: public QThread
{
    QTcpSocket * clientSocket;

public:
    ClientThread(QTcpSocket * clientSocket);

    void run()
    {
        qDebug() << "newThread";
        RequestsHandler handler("../../data/sql/data_sqlite");
        while (true)
        {
            qDebug() << "New request";
            if (!clientSocket->waitForReadyRead(20000))
            {
                qDebug() << "No requests \n";
                break;
            }
            QByteArray inputBytes = clientSocket->readAll();
            QDomDocument clientRequest;
            clientRequest.setContent(inputBytes);
            qDebug() << QString::fromStdString(inputBytes.toStdString());
            QDomElement el = clientRequest.documentElement();

            if (el.tagName() == "requests")
            {
                QDomAttr atr = el.attributeNode("request");
                QString st = atr.value();
                if (st == "signin")
                {
                    handler.userSignIn(clientRequest, clientSocket);
                    break;
                }
                else if (st == "login")
                {
                    handler.userLogIn(clientRequest, clientSocket);
                }
                else if (st == "getSomeTourOperators")
                {
                    handler.getSomeTourOperators(clientRequest, clientSocket);
                }
                else if (st == "numberOfTourOperators")
                {
                    handler.numberOfTourOperators(clientRequest, clientSocket);
                }
                else if (st == "editTourOperator")
                {
                    handler.editTourOperator(clientRequest, clientSocket);
                }
                else if (st == "addTourOperator")
                {
                     handler.addTourOperator(clientRequest, clientSocket);
                }
                else if (st == "deleteTourOperator")
                {
                     handler.deleteTourOperator(clientRequest, clientSocket);
                }
                else if (st == "savePic")
                {
                     handler.savePic(clientRequest, clientSocket);
                }
                else if (st == "downloadPic")
                {
                    handler.downloadPic(clientRequest, clientSocket);
                }
                else if (st == "addVacation")
                {
                    handler.addVacation(clientRequest, clientSocket);
                }
                else if (st == "Vacations_tourop")
                {
                    handler.vacationsTourop(clientRequest, clientSocket);
                }
                else if (st == "Vacations")
                {
                    handler.vacations(clientRequest, clientSocket);
                }
                else if (st == "VacationInsert")
                {
                    handler.vacationInsert(clientRequest, clientSocket);
                }
                else if (st == "deleteVacation")
                {
                    handler.deleteVacationLink(clientRequest, clientSocket);
                }
            }
            closeSocket(*clientSocket);
            delete clientSocket;
            break;
        }
    }


    void closeSocket(QTcpSocket & client_socket)
    {
        if (client_socket.state() == QTcpSocket::SocketState::ConnectedState)
        {
            qDebug() << "Start disconnecting from server...";
            client_socket.disconnect();
            if (!client_socket.waitForDisconnected(1000))
            {
                qDebug() << "Disconnected.";
            }
            else
            {
                qDebug() << "Disconnecting timeout (1000).";
            }
        }
        else
        {
            qDebug() << "Already disconnected from server.";
        }
    }
};
#endif // CLIENTTHREAD_H
