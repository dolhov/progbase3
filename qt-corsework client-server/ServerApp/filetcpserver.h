#ifndef FILETCPSERVER_H
#define FILETCPSERVER_H

#include <QFile>
#include "medium.h"
#include "fileipc.h"

class FileTCPServer
{
   Medium medium_;

   QFile inConnectFile_;

public:
   FileTCPServer(const Medium & medium);

   void listen(int thisId);
   FileIpc * newConnection();
};

#endif // FILETCPSERVER_H
