#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>
#include <QtXml>
#include <QThread>
#include "FileStorage.h"
#include "sqlitestorage.h"
#include "user.h"
#include "requestshandler.h"
#include "clientthread.h"

//using namespace std;


//void closeSocket(QTcpSocket & client_socket);

int main()
{
    QTcpServer server;
    int port = 9999;
    if (!server.listen(QHostAddress::Any, port))
    {
        qDebug() << "Can't listen on: " << port << "\n";
    }
    qDebug() << "Server is on port: " << port << "\n";
    QVector<ClientThread *> threads;
    while (true)
    {
        if (!server.waitForNewConnection(2000))
        {
            qDebug() << "No connections \n";
            continue;
        }
        QTcpSocket * clientSocket = server.nextPendingConnection();
        ClientThread * clientThread = new ClientThread(clientSocket);
        clientThread->start();
        threads.push_back(clientThread);
        continue;
    }
    server.close();
    for (int i = 0; i < threads.size(); i++)
    {
        delete threads[i];
    }
}







//void closeSocket(QTcpSocket & client_socket)
//{
//   if (client_socket.state() == QTcpSocket::SocketState::ConnectedState)
//   {
//       qDebug() << "Start disconnecting from server...";
//       client_socket.disconnect();
//       if (!client_socket.waitForDisconnected(1000))
//       {
//           qDebug() << "Disconnected.";
//       }
//       else
//       {
//           qDebug() << "Disconnecting timeout (1000).";
//       }
//   }
//   else
//   {
//       qDebug() << "Already disconnected from server.";
//   }
//}

