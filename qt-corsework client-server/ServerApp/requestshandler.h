#ifndef REQUESTSHANDLER_H
#define REQUESTSHANDLER_H

#include "sqlitestorage.h"
#include <QTcpSocket>
#include <QHostAddress>
#include <QtXml>
#include <QThread>
#include "FileStorage.h"
#include "sqlitestorage.h"
#include "user.h"

class RequestsHandler
{
    SqliteStorage * storage_ = nullptr;
public:
    RequestsHandler(QString path);
    ~RequestsHandler();

    void userSignIn(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void userLogIn(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void getSomeTourOperators(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void numberOfTourOperators(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void editTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void addTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void deleteTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void savePic(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void downloadPic(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void addVacation(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void vacationsTourop(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void vacations(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void vacationInsert(QDomDocument clientRequest, QTcpSocket * clientSocket);
    void deleteVacationLink(QDomDocument clientRequest, QTcpSocket * clientSocket);
};

#endif // REQUESTSHANDLER_H
