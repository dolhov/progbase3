#include "requestshandler.h"

RequestsHandler::RequestsHandler(QString path)
{
    SqliteStorage * stor;
    stor = new SqliteStorage(path.toStdString());
    storage_ = stor;
    storage_->open();
}


RequestsHandler::~RequestsHandler()
{
    delete storage_;
}

void RequestsHandler::userSignIn(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QString st;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                if (storage_->isUniqueUser(domElement.attribute("username", "").toStdString()))
                {
                    storage_->insertUser(domElement.attribute("username", "").toStdString(), domElement.attribute("password", "").toStdString());
                    st = "Succefull sign in";

                }
                else {
                    st = "Nikname is already in use";

                }
                clientSocket->write(st.toUtf8());
                if (!clientSocket->waitForBytesWritten(3000)){
                    qDebug() << "Fail to write";
                }
            }
        }
    }
}


void RequestsHandler::userLogIn(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QDomDocument Response;
    QDomElement root = Response.createElement("response");
    Response.appendChild(root);
    QString st;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                optional<User> opt = storage_->getUserAuth(domElement.attribute("username", "").toStdString(), domElement.attribute("password", "").toStdString());
                if (opt)
                {
                    int user_id = opt.value().id;
                    qDebug() << "User id" << user_id << "loged";
                    QDomElement user = Response.createElement("user");
                    user.setAttribute("user_id", user_id);
                    root.appendChild(user);
                }
                else
                {
                    QDomElement user = Response.createElement("user");
                    user.setAttribute("user_id", -1);
                    root.appendChild(user);
                }

                clientSocket->write(Response.toByteArray());
                if (!clientSocket->waitForBytesWritten(3000)){
                    qDebug() << "Fail to write";
                }
            }
        }
    }
}

void RequestsHandler::getSomeTourOperators(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    int user_id;
    int limit;
    int offset;
    int attribute;
    std::string parameter;
    vector<tour_operator> tour_ops;
    QString st;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                user_id = domElement.attribute("user_id").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "page_parameters")
            {
                limit = domElement.attribute("limit", "0").toInt();
                offset = domElement.attribute("offset", "0").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "search_parameters")
            {
                attribute = domElement.attribute("attribute").toInt();
                parameter = domElement.attribute("parameter").toStdString();
            }
            tour_ops = storage_->searchSomeUserTourOperators(user_id,limit,offset, static_cast<Attributes>(attribute), parameter);
            QDomDocument doc;
            QDomElement root = doc.createElement("tour_operators");
            doc.appendChild(root);
            for (int i = 0; i < tour_ops.size(); i++)
            {
                QDomElement ent = doc.createElement("tour_operator");
                ent.setTagName("tour_operator");
                ent.setAttribute("id", tour_ops[i].id);
                ent.setAttribute("name", QString::fromStdString(tour_ops[i].name));
                ent.setAttribute("city", QString::fromStdString(tour_ops[i].city));
                ent.setAttribute("vacation", QString::fromStdString(tour_ops[i].vacation));
                ent.setAttribute("cost", tour_ops[i].cost);
                ent.setAttribute("currency", QString::fromStdString(tour_ops[i].currency));
                root.appendChild(ent);
            }
            clientSocket->write(doc.toByteArray());
            if (!clientSocket->waitForBytesWritten(3000)){
                qDebug() << "Fail to write";
            }
        }
    }
}


void RequestsHandler::numberOfTourOperators(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    int user_id;
    int attribute;
    std::string parameter;
    vector<tour_operator> tour_ops;
    QString st;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                user_id = domElement.attribute("user_id").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "search_parameters")
            {
                attribute = domElement.attribute("attribute").toInt();
                parameter = domElement.attribute("parameter").toStdString();
            }
            int size = storage_->numberOfUserTourOperators(user_id, static_cast<Attributes>(attribute), parameter);
            clientSocket->write(QString::fromStdString(std::to_string(size)).toUtf8());
            if (!clientSocket->waitForBytesWritten(3000)){
                qDebug() << "Fail to write";
            }
        }
    }
}


void RequestsHandler::editTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QString st;
    tour_operator tourop;
    int user_id;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                user_id = domElement.attribute("user_id").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "tour_operator")
            {
                tourop.name = domElement.attribute("name", "0").toStdString();
                tourop.id = domElement.attribute("id", "0").toInt();
                tourop.city = domElement.attribute("city", "0").toStdString();
                tourop.cost = domElement.attribute("cost", "0").toInt();
                tourop.currency = domElement.attribute("currency", "0").toInt();
                tourop.vacation = domElement.attribute("vacation", "0").toStdString();
                if(storage_->updateTourOperator(tourop))
                {
                    clientSocket->write(QString::fromStdString("Updated succesfully").toUtf8());
                    if (!clientSocket->waitForBytesWritten(3000)){
                        qDebug() << "Fail to write";
                    }
                }
            }
        }
    }
}


void RequestsHandler::addTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QString st;
    tour_operator tourop;
    int user_id;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                user_id = domElement.attribute("user_id").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "tour_operator")
            {
                tourop.name = domElement.attribute("name", "0").toStdString();
                tourop.id = domElement.attribute("id", "0").toInt();
                tourop.city = domElement.attribute("city", "0").toStdString();
                tourop.cost = domElement.attribute("cost", "0").toInt();
                tourop.currency = domElement.attribute("currency", "0").toInt();
                tourop.vacation = domElement.attribute("vacation", "0").toStdString();
                tourop.user_id = domElement.attribute("user_id", "0").toInt();
                if(storage_->insertTourOperator(tourop))
                {
                    clientSocket->write(QString::fromStdString("Inserted succesfully").toUtf8());
                    if (!clientSocket->waitForBytesWritten(3000)){
                        qDebug() << "Fail to write";
                    }
                }
            }
        }
    }
}


void RequestsHandler::deleteTourOperator(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QString st;
    tour_operator tourop;
    int user_id;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "user")
            {
                user_id = domElement.attribute("user_id").toInt();
                domElement = domnode.nextSibling().toElement();
                domnode = domnode.nextSibling();
            }
            if (domElement.tagName() == "tour_operator")
            {
                tourop.id = domElement.attribute("id", "0").toInt();

                if(storage_->removeTourOperator(tourop.id))
                {
                    clientSocket->write(QString::fromStdString("Deleted succesfully").toUtf8());
                    if (!clientSocket->waitForBytesWritten(3000)){
                        qDebug() << "Fail to write";
                    }
                }
            }
        }
    }
}


void RequestsHandler::savePic(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QDomNode domnode = clientRequest.documentElement();
    QDomElement domElement = domnode.toElement();
    int user_id = domElement.attribute("user_id").toInt();
    std::string st = "ready";
    clientSocket->write(QString::fromStdString(st).toUtf8());
    if (!clientSocket->waitForBytesWritten(3000)){
        qDebug() << "Fail to write";
    }
    if (!clientSocket->waitForReadyRead(30000))
    {
        qDebug() << "No requests \n";
        return;
    }
    QByteArray inputBytes = clientSocket->readAll();
    storage_->savePicture(user_id, inputBytes);
}


void RequestsHandler::downloadPic(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QDomNode domnode = clientRequest.documentElement();
    QDomElement domElement = domnode.toElement();
    int user_id = domElement.attribute("user_id").toInt();

    QByteArray outputBytes = storage_->getPicture(user_id);
    clientSocket->write(outputBytes);
    if (!clientSocket->waitForBytesWritten(3000)){
        qDebug() << "Fail to write";
    }
}


void RequestsHandler::addVacation(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    QString st;
    vacation vac;
    int user_id;
    QDomNode domnode = clientRequest.documentElement();
    domnode = domnode.firstChild();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            if (domElement.tagName() == "vacation")
            {
                vac.city = domElement.attribute("city", "0").toStdString();
                vac.duration = domElement.attribute("duration", "0").toInt();
                vac.hotel_name = domElement.attribute("hotel_name", "0").toStdString();
                if(storage_->insertVacation(vac))
                {
                    clientSocket->write(QString::fromStdString("Inserted succesfully").toUtf8());
                    if (!clientSocket->waitForBytesWritten(3000)){
                        qDebug() << "Fail to write";
                    }
                }
            }
        }
    }
}

void RequestsHandler::vacationsTourop(QDomDocument clientRequest, QTcpSocket * clientSocket)
{
    vector<vacation> vacs;
    QString st = clientRequest.toString();
    QDomNode domnode = clientRequest.documentElement();
    if (domnode.isElement())
    {
        QDomElement domElement = domnode.toElement();
        st = domElement.toText().data();
        if (!domElement.isNull())
        {
            int tourop_id = domElement.attribute("tourop_id").toInt();
            vacs = storage_->getAllTourOperatorsVacations(tourop_id);
            QDomDocument doc;
            QDomElement root = doc.createElement("vacations");
            doc.appendChild(root);
            for (int i = 0; i < vacs.size(); i++)
            {
                QDomElement ent = doc.createElement("vacation");
                ent.setTagName("vacation");
                ent.setAttribute("id", vacs[i].id);
                ent.setAttribute("city", QString::fromStdString(vacs[i].city));
                ent.setAttribute("duration", vacs[i].duration);
                ent.setAttribute("hotel_name", QString::fromStdString(vacs[i].hotel_name));
                root.appendChild(ent);
            }
            qDebug() << "Answer: " << doc.toString();
            clientSocket->write(doc.toByteArray());
            if (!clientSocket->waitForBytesWritten(10000)){
                qDebug() << "Fail to write";
            }
        }
    }
}


    void RequestsHandler::vacations(QDomDocument clientRequest, QTcpSocket * clientSocket)
    {
        vector<vacation> vacs;
        QString st = clientRequest.toString();
        QDomNode domnode = clientRequest.documentElement();
        if (domnode.isElement())
        {
            QDomElement domElement = domnode.toElement();
            st = domElement.toText().data();
            if (!domElement.isNull())
            {
                int tourop_id = domElement.attribute("tourop_id").toInt();
                vacs = storage_->getAllVacations();
                QDomDocument doc;
                QDomElement root = doc.createElement("vacations");
                doc.appendChild(root);
                for (int i = 0; i < vacs.size(); i++)
                {
                    QDomElement ent = doc.createElement("vacation");
                    ent.setTagName("vacation");
                    ent.setAttribute("id", vacs[i].id);
                    ent.setAttribute("city", QString::fromStdString(vacs[i].city));
                    ent.setAttribute("duration", vacs[i].duration);
                    ent.setAttribute("hotel_name", QString::fromStdString(vacs[i].hotel_name));
                    root.appendChild(ent);
                }
                qDebug() << "Answer: " << doc.toString();
                clientSocket->write(doc.toByteArray());
                if (!clientSocket->waitForBytesWritten(10000)){
                    qDebug() << "Fail to write";
                }
            }
        }
    }


    void RequestsHandler::vacationInsert(QDomDocument clientRequest, QTcpSocket * clientSocket)
    {
        QString st = clientRequest.toString();
        QDomNode domnode = clientRequest.documentElement();
        if (domnode.isElement())
        {
            QDomElement domElement = domnode.toElement();
            st = domElement.toText().data();
            if (!domElement.isNull())
            {
                int tourop_id = domElement.attribute("tourop_id").toInt();
                int vacation_id = domElement.attribute("vacation_id").toInt();
                storage_->insertTourOperatorVacation(tourop_id, vacation_id);
                //                    qDebug() << "Answer: " << doc.toString();
                //                    clientSocket->write(doc.toByteArray());
                //                    if (!clientSocket->waitForBytesWritten(10000)){
                //                        qDebug() << "Fail to write";
                //                    }
            }
        }
    }


    void RequestsHandler::deleteVacationLink(QDomDocument clientRequest, QTcpSocket * clientSocket)
    {
        QString st = clientRequest.toString();
        QDomNode domnode = clientRequest.documentElement();
        if (domnode.isElement())
        {
            QDomElement domElement = domnode.toElement();
            st = domElement.toText().data();
            if (!domElement.isNull())
            {
                int tourop_id = domElement.attribute("tourop_id").toInt();
                int vacation_id = domElement.attribute("vacation_id").toInt();
                storage_->removeTourOperatorVacation(tourop_id, vacation_id);
            }

        }
    }
