﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QAction>

const unsigned int PAGE_SIZE = 10;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->actionNew_Storage_2->setShortcut(QKeySequence::New);
    ui->actionNew_Storage_2->setStatusTip(tr("Create a new file"));
    connect(ui->actionNew_Storage_2, &QAction::triggered, this, &MainWindow::actionNew_Storage_2);
    ui->actionOpen_Storage_2->setShortcuts(QKeySequence::Open);
    ui->actionOpen_Storage_2->setStatusTip(tr("Open a file"));
    connect(ui->actionOpen_Storage_2, &QAction::triggered, this, &MainWindow::actionOpen_Storage_2);
    ui->actionLogout->setStatusTip(tr("Finish current session"));
    connect(ui->actionLogout, &QAction::triggered, this, &MainWindow::actionLogout);


    connect(ui->actionAdd, &QAction::triggered, this, &MainWindow::actionAdd);
    ui->actionAdd->setStatusTip(tr("Add new tour operator"));
    connect(ui->actionEdit, &QAction::triggered, this, &MainWindow::actionEdit);
    ui->actionEdit->setStatusTip(tr("Edit selected tour operator"));
    connect(ui->actionRemove, &QAction::triggered, this, &MainWindow::actionRemove);
    ui->actionEdit->setStatusTip(tr("Remove selected tour operator"));
    ui->pushButton->setVisible(false);
    displayTourOperators(getTourOperatorsForPage(current_page));
}

MainWindow::~MainWindow()
{
    if (storage_ != nullptr)
    {
        delete storage_;
    }
    delete ui;
}


void MainWindow::actionOpen_Storage_2()
{
    ui->listWidget->clear();
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "",                // directory to start with
                "SQLITE (*.sqlite);;All Files (*)");  // file name filter
    qDebug() << fileName;
    if (storage_ != nullptr)
    {

        delete storage_;
        storage_ = nullptr;
    }

    QFile fl;
    fl.setFileName(fileName);
    if (fl.exists())
    {
        SqliteStorage * stor;
        stor = new SqliteStorage(fileName.toStdString());
        storage_ = stor;
        storage_->open();
        if (!this->logIn())
        {
            delete storage_;
            storage_ = nullptr;
        }
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "Can't open a storage");
    }
}


void MainWindow::actionNew_Storage_2()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new_storage";
    QString folder_path = dialog.getSaveFileName(
                this,
                "Select New Storage Folder",
                current_dir + "/" + default_name,
                "Folders");
    qDebug() << folder_path;
    SqliteStorage * stor;
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    folder_path += ".sqlite";
    db.setDatabaseName(folder_path);
    if (!db.open())
    {
        qDebug() << "Error creating new database: " << folder_path;
    }
    QString st = "CREATE TABLE links ("
                 "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
                 "tour_operator_id	INTEGER,"
                 "vacation_id	INTEGER,"
                 "FOREIGN KEY(vacation_id) REFERENCES vacations(id))";

    QSqlQuery qry;
    qry.exec(st);
    st = "CREATE TABLE tour_operators ("
         "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
         "name	TEXT,"
         "city	TEXT,"
         "vacation	TEXT,"
         "cost	INTEGER,"
         "currency	TEXT,"
         "user_id	INTEGER,"
         "FOREIGN KEY(user_id) REFERENCES users(id))";

    qry.exec(st);
    st = "CREATE TABLE users ("
         "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
         "username	TEXT,"
         "password_hash	TEXT)";

    qry.exec(st);
    st = "CREATE TABLE vacations ("
         "id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
         "city	TEXT,"
         "duration	INTEGER,"
         "hotel_name	TEXT)";
    qry.exec(st);
    stor = new SqliteStorage(folder_path.toStdString());
    delete this->storage_;
    this->storage_ = stor;
    stor->open();
    this->actionOpen_Storage_2();
}

void  MainWindow::actionAdd()
{
    if (storage_ != nullptr)
    {
        AddTourOperator dialog(this);
        int status = dialog.exec();
        qDebug() << "status: " << status;
        if (status == 1)
        {
            tour_operator tourop = dialog.data();
            tourop.user_id = this->user_loged_id;
            tourop.id = storage_->insertTourOperator(tourop);

            qDebug() << QString::fromStdString(tourop.name) << " | " << QString::fromStdString(tourop.vacation);

            QString text = QString::fromStdString(tourop.name);
            QListWidgetItem * new_item = new QListWidgetItem(text);
            QVariant var = QVariant::fromValue(tourop.id);
            new_item->setData(Qt::UserRole, var);
            userListSize(this->user_loged_id);
            this->displayTourOperators(getTourOperatorsForPage(current_page));
            //ui->listWidget->addItem(new_item);
        }
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "Open a storage first");

    }
}

void MainWindow::actionEdit()
{
    QList list = ui->listWidget->selectedItems();
    if (storage_ != nullptr)
    {
        if (list.size() == 0)
        {
            QMessageBox::information(
                        this,
                        "Information",
                        "Choose entity");
        }
        else
        {
            QListWidgetItem * item = list.at(list.size() - 1);
            QVariant var = item->data(Qt::UserRole);
            tour_operator tourops = storage_->getTourOperatorById(var.toInt()).value();

            Edit dialog(this);
            dialog.SetStorage(this->storage_);
            dialog.SetTourOperatorId(tourops.id);
            dialog.DisplayVacations();
            int status = dialog.exec();
            qDebug() << "status: " << status;
            if (status == 1)
            {

                //            QVariant var = item->data(Qt::UserRole);
                //            QString data = var.toString();

                tour_operator tourop = dialog.data();
                if (tourops.name != tourop.name && tourop.name != "")
                {
                    tourops.name = tourop.name;
                }
                if (tourops.city != tourop.city && tourop.city != "")
                {
                    tourops.city = tourop.city;
                }
                if (tourops.vacation != tourop.vacation && tourop.vacation != "")
                {
                    tourops.vacation = tourop.vacation;
                }
                if (tourops.currency != tourop.currency && tourop.currency != "")
                {
                    tourops.currency = tourop.currency;
                }
                if (tourops.cost != tourop.cost && tourop.cost != 0)
                {
                    tourops.cost = tourop.cost;
                }
                storage_->updateTourOperator(tourops);

                this->displayTourOperators(getTourOperatorsForPage(current_page));
                ui->label_3->setText("");
                ui->label_4->setText("");
                ui->label_8->setText("");
                ui->label_9->setText("");
                ui->label_10->setText("");
            }

        }
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "Open a storage first");

    }
}

void  MainWindow::actionRemove()
{
    if (storage_ != nullptr)
    {
        QList list = ui->listWidget->selectedItems();
        if (list.size() == 0)
        {
            QMessageBox::information(
                        this,
                        "Information",
                        "Choose entity");
        }
        else
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(
                        this,
                        "On delete",
                        "Are you sure?",
                        QMessageBox::Yes|QMessageBox::No);
            if (reply == QMessageBox::Yes) {
                qDebug() << "Yes was clicked";
                QListWidgetItem * item = list.at(list.size() - 1);
                QVariant var = item->data(Qt::UserRole);
                int tourop_id = var.toInt();
                vector<vacation> vacs = storage_->getAllTourOperatorsVacations(tourop_id);
                for (int i = 0; i < vacs.size(); i++)
                {
                    storage_->removeTourOperatorVacation(tourop_id, vacs[i].id);
                }
                storage_->removeTourOperator(var.toInt());
                QListWidgetItem * it = ui->listWidget->takeItem(ui->listWidget->currentRow());
                delete it;
                userListSize(this->user_loged_id);
            } else {
                qDebug() << "Yes was *not* clicked";
            }

            //            QVariant var = item->data(Qt::UserRole);
            //            QString data = var.toString();
        }

    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "Open a storage first");
    }
}



void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    QVariant var = item->data(Qt::UserRole);
    tour_operator tourop = storage_->getTourOperatorById(var.toInt()).value();
    ui->label_3->setText(QString::fromStdString(tourop.name));
    ui->label_4->setText(QString::fromStdString(tourop.city));
    ui->label_8->setText(QString::fromStdString(tourop.vacation));
    ui->label_9->setText(QString::fromStdString(std::to_string(tourop.cost)));
    ui->label_10->setText(QString::fromStdString(tourop.currency));
}


void MainWindow::actionLogout()
{
    if (this->user_loged_id != -1)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "On logout",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {
            ui->listWidget->clear();
            ui->label_3->setText("");
            ui->label_4->setText("");
            ui->label_8->setText("");
            ui->label_9->setText("");
            ui->label_10->setText("");
            this->user_loged_id = -1;
            this->current_page = 1;
            this->displayTourOperators(getTourOperatorsForPage(current_page));
            this->logIn();
        }
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "You are not loged");
    }

}


bool MainWindow::logIn()
{
    auntefication au(this);
    au.setStorage(this->storage_);
    int status = au.exec();
    storage_->open();
    qDebug() << "status: " << status;
    vector<string> name_password;
    if (status == 1)
    {
        name_password = au.login();
        if (name_password.size() == 0)
        {
            name_password = au.signIn();
        }
        this->user_loged_id = -1;

    }
    if (name_password.size() != 0)
    {
        qDebug() << QString::fromStdString(name_password[0]);
        optional<User> opt = storage_->getUserAuth(name_password[0], name_password[1]);
        User usr;
        if (opt)
        {
            usr = opt.value();
            this->user_loged_id = usr.id;
            this->userListSize(user_loged_id);
            ui->pushButton_5->setVisible(false);
            this->displayTourOperators(getTourOperatorsForPage(current_page));
            return true;
        }
        else
        {
            QMessageBox::information(
                        this,
                        "Information",
                        "Invalid username or password");
            return false;
        }
    }
}


void MainWindow::displayTourOperators(std::vector<tour_operator> tourops)
{
    ui->listWidget->clear();
    if(user_loged_id != -1)
    {
        for (tour_operator & tours : tourops)
        {
            QString text = QString::fromStdString(tours.name);
            QListWidgetItem * new_item = new QListWidgetItem(text);
            QVariant var = QVariant::fromValue(tours.id);
            new_item->setData(Qt::UserRole, var);
            ui->listWidget->addItem(new_item);
        }
    }
    else
    {
        ui->pushButton_4->setVisible(false);
        ui->pushButton_5->setVisible(false);
    }
}

std::vector<tour_operator> MainWindow::getTourOperatorsForPage(unsigned int page, Attributes atr, std::string parameter)
{
    std::vector<tour_operator> tourops;
    if (user_loged_id != -1)
    {
        if (atr == Attributes::EMPTY)
        {
            tourops = storage_->getSomeUserTourOperators(this->user_loged_id, PAGE_SIZE, (page - 1) * PAGE_SIZE);
        }
        else
        {
            tourops = storage_->searchSomeUserTourOperators(user_loged_id, PAGE_SIZE, (page - 1) * PAGE_SIZE, atr, parameter);
        }
    }
    return tourops;
}


void MainWindow::userListSize(int user_id, Attributes atr, std::string parameter)
{
    unsigned int size = storage_->numberOfUserTourOperators(user_id, atr, parameter);
    max_page =  size / PAGE_SIZE + 1;
    if (current_page == max_page)
    {
        ui->pushButton_4->setVisible(false);
    }
    else
    {
        ui->pushButton_4->setVisible(true);
    }
    QString st = "/  ";
    st += QString::fromStdString(std::to_string(size/PAGE_SIZE + 1));
    ui->label_16->setText(st);
}

// next button
void MainWindow::on_pushButton_4_clicked()
{
    if (user_loged_id != -1)
    {
        if (current_page == 1)
        {
            ui->pushButton_5->setVisible(true);
        }
        current_page++;
        displayTourOperators(getTourOperatorsForPage(current_page, current_search_atr, current_search_parametr));
        if (current_page == max_page)
        {
            ui->pushButton_4->setVisible(false);
        }
        ui->label_15->setText(QString::fromStdString(std::to_string(current_page)));
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "You are'not logged in");
    }
}

// previous button
void MainWindow::on_pushButton_5_clicked()
{
    if (user_loged_id != -1)
    {
        if (current_page == max_page)
        {
            ui->pushButton_4->setVisible(true);
        }
        current_page--;
        displayTourOperators(getTourOperatorsForPage(current_page, current_search_atr, current_search_parametr));
        if (current_page == 1)
        {
            ui->pushButton_5->setVisible(false);
        }
        ui->label_15->setText(QString::fromStdString(std::to_string(current_page)));
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "You are'not logged in");
    }
}

// search button
void MainWindow::on_pushButton_6_clicked()
{
    ui->pushButton_6->
    if (user_loged_id != -1)
    {
        Attributes atr = static_cast<Attributes> (ui->comboBox->currentIndex());
        QString parameter = ui->lineEdit->text();
        current_page = 1;
        ui->label_15->setText(QString::fromStdString(to_string(current_page)));
        ui->pushButton_5->setVisible(false);
        current_search_atr = atr;
        current_search_parametr = parameter.toStdString();
        vector<tour_operator> tour_operators = this->storage_->searchSomeUserTourOperators(user_loged_id, PAGE_SIZE, (current_page - 1) * PAGE_SIZE, atr, parameter.toStdString());
        userListSize(this->user_loged_id, atr, parameter.toStdString());
        displayTourOperators(tour_operators);
        ui->pushButton->setVisible(true);
    }
    else
    {
        QMessageBox::information(
                    this,
                    "Information",
                    "You are'not logged in");
    }
}

// Back to tour operators button
void MainWindow::on_pushButton_clicked()
{
    current_page = 1;
    current_search_atr = Attributes::EMPTY;
    current_search_parametr = "";
    userListSize(user_loged_id);
    this->displayTourOperators(getTourOperatorsForPage(current_page));
    ui->pushButton->setVisible(false);
}
