#ifndef AUNTEFICATION_H
#define AUNTEFICATION_H

#include <QDialog>
#include <vector>
#include <string>
#include <QDebug>
#include "siginin.h"
#include "sqlitestorage.h"

namespace Ui {
class auntefication;
}

class auntefication : public QDialog
{
    Q_OBJECT
    std::vector<std::string> newNamePassword;
    Storage * stor_;

public:
    explicit auntefication(QWidget *parent = 0);
    ~auntefication();
    void setStorage(Storage * stor_);
    std::vector<std::string> login();
    std::vector<std::string> signIn();
private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

private:
    Ui::auntefication *ui;
};

#endif // AUNTEFICATION_H
