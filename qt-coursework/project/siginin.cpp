#include "siginin.h"
#include "ui_siginin.h"

SiginIn::SiginIn(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SiginIn)
{
    ui->setupUi(this);
}

SiginIn::~SiginIn()
{
    delete ui;
}




std::vector<std::string> SiginIn::namePassword()
{
    return this->name_password;
}

void SiginIn::on_pushButton_2_clicked()
{
    if (!st->isUniqueUser(ui->lineEdit->text().toStdString()))
    {
        QMessageBox::information(
            this,
            "Information",
            "This username is already in use");
    }
    else if (ui->lineEdit_2->text() != ui->lineEdit_3->text())
    {
        QMessageBox::information(
            this,
            "Information",
            "Passwords are not same. Re-enter passwords");
    }
    else if (ui->lineEdit_2->text().size() < 6)
    {
        QMessageBox::information(
            this,
            "Information",
            "Password should be at least 6 charachters long");
    }
    else
    {

         qDebug() << ui->lineEdit->text();
         this->st->insertUser(ui->lineEdit->text().toStdString(), ui->lineEdit_2->text().toStdString());
         QMessageBox::information(
                this,
                "Information",
                "Succesfull signin");

        this->accept();
    }
}

void SiginIn::on_pushButton_clicked()
{
    this->close();
}

 void SiginIn::setStorage(Storage * stor)
 {
     this->st = stor;
 }
