#ifndef LIBCSVLAB_GLOBAL_H
#define LIBCSVLAB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LIBCSVLAB_LIBRARY)
#  define LIBCSVLABSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBCSVLABSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBCSVLAB_GLOBAL_H
